# ubuntu-node-docker

This repo mostly exists because MongoDB does not provide ARM64 build for Debian (ubuntu only) but the Node team does not provide Ubuntu images

In case you want to execute tests on ARM64 GitLab workers with MongoDB and node you should use images like that


# Repository

Repository is located at https://gitlab.com/adaptivestone/ubuntu-node

# Build

To build an image use

```
docker buildx prune
docker buildx build --platform linux/amd64,linux/arm64 -t registry.gitlab.com/adaptivestone/ubuntu-node:latest --push .
docker buildx build --platform linux/amd64,linux/arm64 -t systerr/ubuntu-node:latest --push .
```

Where to find build images?

Docker hub:

```
systerr/ubuntu-node
```

and

Gitlab registry

```
registry.gitlab.com/adaptivestone/ubuntu-node
```
